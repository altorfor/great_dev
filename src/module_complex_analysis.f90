module module_complex_analysis
  
  use module_param
  use module_background
  
  implicit none
  private
  type, public :: mode_complex_t
      !> size of the grid
     integer :: iR
     !> Coefficient matrixes
     complex*8, dimension (:,:,:), allocatable :: Ml, Mdr, Mdr2
     complex*8, dimension (:,:,:), allocatable :: D, Dp, Dm
  end type mode_complex_t
  
  public :: Initialize_complex_matrix
  public :: Free_complex_matrix
  public :: Compute_coeeficients_matrix
  public :: complex_integration

contains

  !=========================================================================
  ! Initializes coefficients matrix
  !=========================================================================

  subroutine Initialize_complex_matrix (data, param, coeff, ierr)

    ! -----  Arguments  --------
    ! input
    type(BGData_t), intent (inout) :: data
    type (parameters_t), intent (inout) :: param    
    ! output
    type (mode_complex_t), intent (inout) :: coeff
    integer, intent (out) :: ierr
    ! --------------------------
    print*,"Initializing"
    ierr = 0
    coeff%iR = data%iR

    allocate (coeff%Ml (1:4, 1:4, 1:coeff%iR))
    allocate (coeff%Mdr (1:4, 1:4, 1:coeff%iR))
    allocate (coeff%Mdr2 (1:4, 1:4, 1:coeff%iR))
    allocate (coeff%D (1:4, 1:4, 1:coeff%iR))
    allocate (coeff%Dp (1:4, 1:4, 1:coeff%iR))
    allocate (coeff%Dm (1:4, 1:4, 1:coeff%iR))

    coeff%Ml = 0.0d0
    coeff%Mdr = 0.0d0
    coeff%Mdr2 = 0.0d0
    coeff%D = 0.0d0
    coeff%Dp = 0.0d0
    coeff%Dm = 0.0d0
  end subroutine Initialize_complex_matrix

  
  !=========================================================================
  ! Free coefficients matrix
  !=========================================================================

  subroutine Free_complex_matrix (coeff, ierr)

    ! -----  Arguments  --------
    type (mode_complex_t), intent (inout) :: coeff
    ! output
    integer, intent (out) :: ierr
    ! --------------------------
    print*,"dealocating"

    
    ierr = 0
    coeff%iR = 0
    
    deallocate (coeff%Ml) 
    deallocate (coeff%Mdr) 
    deallocate (coeff%Mdr2)
    deallocate (coeff%D)
    deallocate (coeff%Dp)
    deallocate (coeff%Dm)


  end subroutine Free_complex_matrix

  ! ==============================================================================================================
  ! Computes the coefficient matrix corresponding to the non-Cowling approximation (6x6)
  ! ==============================================================================================================  
  subroutine Compute_coeeficients_matrix (data, param, coeff, ierr)  

    implicit none
    
    ! ------- Arguments -------
    ! input
    type(BGData_t), intent (inout) :: data
    type(parameters_t), intent (inout) :: param
    ! output
    type (mode_complex_t), intent (inout) :: coeff
    integer, intent (out) :: ierr
    ! ----------------------------

    integer :: i,j,k

    !Auxiliary variables
    
 !> temporal varibles (from background)
     real*8, dimension (:), allocatable :: A
     real*8, dimension (:), allocatable :: dA_dr
     real*8, dimension (:), allocatable :: dcs_dr
     real*8, dimension (:), allocatable :: dL2_dr
     real*8, dimension(:), allocatable :: coeff2_dp,coeff2_d0,coeff2_dm
     real*8 :: al,bl,cl,dl
     real*8 :: adr,bdr,cdr,ddr
     real*8 :: adr2,bdr2,cdr2,ddr2
     real*8 :: L2_aux
     integer :: m, g,ll
     
     print*,"computing"


     m = data%m
     g = data%g
     
     !> Allocate variables
     
     allocate(A(1-g : m+g))
     allocate(dA_dr(1-g : m+g))
     allocate(dcs_dr(1-g : m+g))
     allocate(dL2_dr(1-g : m+g))
     allocate(coeff2_dp(1 : m))
     allocate(coeff2_d0(1 : m))
     allocate(coeff2_dm(1: m))
     !> Initialize variables
     A=0.0d0
     dA_dr=0.0d0
     dcs_dr=0.0d0
     dL2_dr=0.0d0

     coeff2_dp = 0.0d0
     coeff2_d0 = 0.0d0
     coeff2_dp = 0.0d0

     do i = 1,data%iR-1
        coeff2_dp(i) = 2.0d0 / (data%r(i+1)-data%r(i-1))*(data%r(i+1)-data%r(i)) 
        coeff2_d0(i) =  2.0d0 / (data%r(i)-data%r(i-1))*(data%r(i)-data%r(i+1))
        coeff2_dm(i) = 2.0d0 / (data%r(i-1)-data%r(i))*(data%r(i-1)-data%r(i+1)) 
     enddo
     
     !> Compute variable A 
     do i = 1, data%m
        A(i) = 2.0d0 + data%r(i)*data%ggrav(i)*data%inv_cs2(i) &
             + 6.0d0*data%r(i)*data%dphi_dr(i)/data%phi(i)
     enddo
     
     !>Boundary conditions
     do i = 1, g
        A(i-1) = -A(i)
     enddo
     do i = 1, g
        A(m+i) = A(m)
     end do

      !> Compute derivatives
     do i = 1, data%m
       dA_dr(i) = A (i + 1) *  data%coeff_dp (i) &
            + A (i) *  data%coeff_d0 (i)&
            + A (i - 1) *  data%coeff_dm (i)
       dcs_dr(i) = data%c_sound_squared (i + 1) *  data%coeff_dp (i) &
            + data%c_sound_squared (i) *  data%coeff_d0 (i) &
            + data%c_sound_squared (i - 1) *  data%coeff_dm (i)
    enddo
    ll=param%l*(param%l+1)
    do i = 1, data%m
       L2_aux = ll*data%alpha(i)/(data%r(i)**2 * data%phi(i)**4)
       dL2_dr(i) = -2.0d0*L2_aux*data%c_sound_squared (i)* data%alpha(i)/data%r(i) &
            + L2_aux*dcs_dr(i) * data%alpha(i) &
            - 2.0d0 * L2_aux * data%c_sound_squared(i)*data%dalpha_dr(i) &
            - 4.0d0 * L2_aux *data%c_sound_squared (i)* data%alpha(i) * data%dphi_dr(i) / data%phi(i)   
    enddo

    !> Compute complex matrix
    !0 0 aa bb
    !0 0 cc dd
    !i 0 0 0
    !0 i 0 0
    ! Check sign of two last rows
    !Linear matrix
    do i=1, data%iR
       al = data%alpha(i)**2/data%phi(i)**4 * ( &
            data%Bstar(i)*data%ggrav(i) - data%c_sound_squared (i)*dA_dr(i) + &
            A(i)*( (1.0d0+3.0d0* data%c_sound_squared (i))*data%ggrav(i) - &
            dcs_dr(i) - data%c_sound_squared (i) * data%dlnq_dr(i) + &
            4.0d0 * data%c_sound_squared (i) * data%dphi_dr(i) / data%phi(i)))
       
       bl = A(i) * data%c_sound_squared (i) * data%alpha(i)**2 /data%phi(i)**4
       
       cl = (1.0d0 + data%c_sound_squared (i))*data%ggrav(i) * data%lamb2(i) * data%inv_cs2(i) &
            - dL2_dr(i) - data%lamb2(i) * data%dlnq_dr(i)
       
       dl = data%lamb2(i)

       !Fill non-zero components
       coeff%Ml(1,3,i) = complex(0,al)
       coeff%Ml(1,4,i) = complex(0,bl)
       coeff%Ml(2,3,i) = complex(0,cl)
       coeff%Ml(2,4,i) = complex(0,dl)
       coeff%Ml(3,1,i) = complex(0,1)
       coeff%Ml(4,2,i) = complex(0,1)
    enddo

    !Print for debugging

    ! 1st derivative matrixx
    do i=1, data%iR
       adr = data%alpha(i)**2/data%phi(i)**4* &
            ( A(i)*data%c_sound_squared (i)  &
            - (1.0d0+3.0d0* data%c_sound_squared (i))*data%ggrav(i)  &
            + dcs_dr(i) + data%c_sound_squared (i) * data%dlnq_dr(i) &
            - 4.0d0 * data%c_sound_squared (i) * data%dphi_dr(i) / data%phi(i))
       
       bdr = data%c_sound_squared (i) * data%alpha(i)**2 /data%phi(i)**4
       
       cdr =  data%lamb2(i)
       

       !Fill non-zero components
       coeff%Mdr(1,3,i) = complex(0,adr)
       coeff%Mdr(1,4,i) = complex(0,bdr)
       coeff%Mdr(2,3,i) = complex(0,cdr)

    enddo

        ! 2nd derivative matrixx
    do i=1, data%iR
       
       adr2 = data%c_sound_squared (i) * data%alpha(i)**2 /data%phi(i)**4

       !Fill non-zero components
       coeff%Mdr2(1,3,i) = complex(0,adr2)

    enddo

    !> Fill matrices
    do k = 1, data%iR
       do j = 1,4
          do i = 1,4
             coeff%D(i,j,k) = coeff%Ml(i,j,k) + coeff%Mdr(i,j,k)* data%coeff_d0(k) + coeff%Mdr2(i,j,k) * coeff2_d0(k)
             coeff%Dp(i,j,k) = coeff%Mdr(i,j,k)* data%coeff_dp(k) + coeff%Mdr2(i,j,k) * coeff2_dp(k)
             coeff%Dm(i,j,k) = coeff%Mdr(i,j,k)* data%coeff_dm(k) + coeff%Mdr2(i,j,k) * coeff2_dp(k)
          enddo
       enddo
    enddo
    do k=1, data%iR
        do j = 1,4
          do i = 1,4
             write(*,*) i,j,k, coeff%D(i,j,k),coeff%Dp(i,j,k), coeff%Dm(i,j,k)
          enddo
       enddo
     enddo
    
    deallocate(A)
    deallocate(dA_dr)
    deallocate(dcs_dr)
    deallocate(dL2_dr)
    deallocate(coeff2_dp)
    deallocate(coeff2_d0)
    deallocate(coeff2_dm)
    
  end subroutine Compute_coeeficients_matrix

  subroutine complex_integration(data, param, coeff, ierr)
    implicit none
    
    ! ------- Arguments -------
    ! input
    type(BGData_t), intent (inout) :: data
    type(parameters_t), intent (inout) :: param

    ! output
    type (mode_complex_t), intent (inout) :: coeff
    integer, intent (out) :: ierr
    ! ----------------------------
    call Initialize_complex_matrix(data,param,coeff,ierr)
    call Compute_coeeficients_matrix (data, param, coeff, ierr) 
    call Free_complex_matrix (coeff, ierr)
    
  end subroutine complex_integration
     
end module module_complex_analysis
